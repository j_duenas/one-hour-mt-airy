// Variables
var themeName = 'OneHourMtAiry',
    gulp = require('gulp'),
    folders = {
        srcTheme: 'src/theme/',
        appTheme: 'app/wp-content/themes/' + themeName + '/'
    },
    dep = {
        newer: require('gulp-newer'),
        imageMin: require('gulp-imagemin'),
        sass: require('gulp-sass'),
        cleanCSS: require('gulp-clean-css'),
        sourceMaps: require('gulp-sourcemaps'),
        autoPrefixer: require('gulp-autoprefixer'),
        concat: require('gulp-concat'),
        uglify: require('gulp-uglify'),
        nsg: require('node-sprite-generator')
    };

// Tasks

/**
 * gulp finalize-images
 * 1. Takes all images inside `src/images` and compresses them via imagemin
 * 2. Outputs compressed images `to app/images`
 */
gulp.task('finalize-images', ['generate-sprites'], function(){
    var output = folders.appTheme + 'images/';
    return gulp.src(folders.srcTheme + 'images/**/*')
        .pipe(dep.newer(output))
        .pipe(dep.imageMin({optimizationLevel: 5}))
        .pipe(gulp.dest(output));
});

gulp.task('generate-sprites', function(){
    var output = folders.appTheme + 'images/';

    dep.nsg({
        src: [
            folders.srcTheme + 'images/sprites/*.png'
        ],
        spritePath: folders.appTheme + 'images/spritesheet.png',
        stylesheet: 'scss',
        stylesheetOptions: {
            prefix: '',
            spritePath: 'images/spritesheet.png'
        },
        stylesheetPath: folders.srcTheme + 'css/modules/_sprites.scss',
        layout: 'packed',
        layoutOptions: {
          padding: 10
        }
    }, function(err){
        console.log('Spritemap generated...' + err);
    })
});

/**
 * gulp sass
 * 1. Takes src/style.scss and compiles it into app/style.css
 * 2. Outputs any errors.
 */
gulp.task('sass', function(){
    return gulp.src(folders.srcTheme + 'css/style.scss')
        .pipe(dep.sourceMaps.init())
        .pipe(dep.sass({outputStyle: 'compressed'}).on('error', dep.sass.logError))
        .pipe(dep.sourceMaps.write('.'))
        .pipe(gulp.dest(folders.appTheme))
});

/**
 * gulp finalize-css
 * 1. Creates a sourcemap appended to style.css
 * 2. Adds autoprefixing to style.css
 * 3. Minimizes CSS down to a single file (2 for sourcemap)
 * 4. Outputs cleaned, final style.css file to app directory
 */
gulp.task('finalize-css', ['sass'], function(){
    return gulp.src(folders.srcTheme + 'style.css')
        .pipe(dep.autoPrefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(dep.cleanCSS())
        .pipe(gulp.dest(folders.appTheme))
});

/**
 * gulp finalize-js
 * 1. Concatenates all .js files in src/js/ into a single file (all.js)
 */
gulp.task('finalize-js', function(){
    var output = folders.appTheme;
    return gulp.src([
            folders.srcTheme + 'js/jquery.featherlight.js',
            folders.srcTheme + 'js/jquery.printelement.js',
            folders.srcTheme + 'js/slick/slick.js'
    ])
        .pipe(dep.concat('all.js'))
        .pipe(dep.uglify())
        .pipe(gulp.dest(output))
});

/**
 * gulp build
 * Runs when ./start.sh is run and when `gulp watch` detects a change.
 * 1. Runs `gulp finalize-images`
 * 2. Runs `gulp finalize-css`
 * 3. Runs `gulp finalize-js`
 * 4. Dumps all outputted files to app/
 */
gulp.task('build', ['finalize-images', 'finalize-css', 'finalize-js'], function(){
    var output = folders.appTheme;
    return gulp.src(folders.srcTheme + '**/*')
        .pipe(dep.newer(output))
        .pipe(gulp.dest(output))
});

/**
 * gulp watch
 * Watches for any changes and triggers tasks as needed.
 */
gulp.task('watch', function(){
   var watchList = [
     folders.srcTheme + '**/*',
     '!' + folders.srcTheme + 'style.css', //ignore style.css
     '!' + folders.srcTheme + 'all.js' //ignore all.js
   ];

    gulp.watch(watchList, ['build']);
});