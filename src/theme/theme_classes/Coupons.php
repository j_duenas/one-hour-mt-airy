<?php
    namespace icong;

    class Coupons{
        protected   $coupons_results,
                    $coupon_fields,
                    $coupons = [];

        public function __construct(){
            $this->get_coupon_fields();
            $this->build_coupons();
        }

        protected function get_coupon_fields(){
            $this->coupon_results = \Timber::get_posts([
                'post_type' => 'coupons'
            ]);

            foreach($this->coupon_results as $coupon_result){
                $this->coupon_fields[] = get_fields($coupon_result->ID);
            }

            return $this->coupon_fields;
        }

        protected function build_coupons(){
            foreach($this->coupon_fields as $coupon){
                $expiration_date = strtotime($coupon['expiration_date']);
                if($expiration_date < time()){
                    continue;
                }
                
                $new_coupon = new Coupon([
                    'id' => rand(1000, 9999),
                    'type' => [
                        'coupon' => $coupon['coupon_type'],
                        'discount' => (isset($coupon['discount_type'])) ? $coupon['discount_type'] : false
                    ],
                    'price' => $coupon['coupon_price'],
                    'title' => $coupon['coupon_title'],
                    'expiration_date' => $expiration_date,
                    'description' => $coupon['coupon_description'],
                    'image' => $coupon['coupon_image']
                ]);
                
                if($new_coupon->type['coupon'] === "offer"){
                    $new_coupon->offer_text = $coupon['offer_text'];
                }
                
                $this->coupons[] = $new_coupon;
            }
        }

        public function get_coupons(){
            return $this->coupons;
        }
    }