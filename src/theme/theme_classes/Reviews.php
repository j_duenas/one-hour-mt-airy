<?php
    namespace icong;

    class Reviews{
        protected   $reviews_results,
                    $review_fields,
                    $reviews = [];

        public function __construct(){
            $this->get_review_fields();
            $this->build_reviews();
        }

        protected function get_review_fields(){
            $this->review_results = \Timber::get_posts([
                'post_type' => 'reviews'
            ]);

            foreach($this->review_results as $review_result){
                $fields = get_fields($review_result->ID);
                $fields['review_id'] = $review_result->ID;
                $this->review_fields[] = $fields;
            }

            return $this->review_fields;
        }

        protected function build_reviews(){
            foreach($this->review_fields as $review){
                $this->reviews[] = new Review([
                    'id' => $review['review_id'],
                    'review_text' => $review['review_text'],
                    'reviewer_name' => $review['reviewer_name']
                ]);
            }
        }

        public function get_reviews(){
            return $this->reviews;
        }
    }