<?php

namespace icong;

class Review extends base{
    public  $id,
            $review_text,
            $reviewer_name;

    public function __construct($data){
        foreach($data as $key => $value){
            $this->setArg($key, $value);
        }
    }
}