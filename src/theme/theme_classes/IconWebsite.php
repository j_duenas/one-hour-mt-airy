<?php

/** @noinspection SpellCheckingInspection */

namespace icong;
use Timber\Menu;
use Timber\Post;
use Timber\Timber;
use Timber\PostQuery;

class IconWebsite{
    protected   $context;
    public      $debug = false,
                $cache = false; // 600

    public function __construct(){
        $this->context = Timber::get_context();
        $this->add_context();
        $this->render_view();
    }

    /**
     * Echoes out $this->context in a semi-readable format
     */
    public function debug(){
        echo '<pre>' . print_r($this->context, true) . '</pre>';
    }

    /**
     * @return bool
     * Checks whether or not a view file exists for the requested page based off of its slug.
     */
    protected function view_file_exists(){
        return file_exists(get_template_directory() . '/views/pages/' . $this->context['post']->slug . '.twig');
    }

    /**
     * @return bool
     * Checks if this is a post-related page (archive, blog page, etc.)
     */
    protected function is_posts(){
        return is_home() || is_archive();
    }


    /**
     * Adds Misc. context
     */
    protected function add_context(){
        // AJAX URL
        $this->context['ajax_url'] = admin_url('admin-ajax.php');

        // Add navigational menus
        $this->context['header_menu']  = new Menu('header-nav');
        $this->context['footer_menu_1'] = new Menu('footer-nav-1');
        $this->context['footer_menu_2']  = new Menu('footer-nav-2');

        // Add footer service area
        $this->context['service_area'] =[
            'carroll_county' => [
                'name' => 'Carroll County',
                'locations' => explode(',', get_field('carroll_county', 'options'))
            ],
            'howard_county' => [
                'name' => 'Howard County',
                'locations' => explode(',', get_field('howard_county', 'options'))
            ],
            'frederick_county' => [
                'name' => 'Frederick County',
                'locations' => explode(',', get_field('frederick_county', 'options'))
            ],
            'montgomery_county' => [
                'name' => 'Montgomery County',
                'locations' => explode(',', get_field('montgomery_county', 'options'))
            ],
            'washington_county' => [
                'name' => 'Washington County',
                'locations' => explode(',', get_field('washington_county', 'options'))
            ]
        ];

        $this->context['columns'] = get_field('columns');

        if(is_page('faqs')){
            $this->context['questions'] = get_field('questions');
        }

        // Add current post's content
        $this->context['post'] = new Post();

        // Add all site options
        $this->context['site_options'] = get_fields('options');

        // Include posts if necessary
        if($this->is_posts()){
            $this->context['posts'] = new PostQuery();
        }

        $this->context['title_override'] = get_field('title_override', $this->context['post']->ID);

        // Nonce
        $this->context['coupon_carousel_nonce'] = wp_create_nonce('coupon_carousel_nonce');

        // Do things specific to the frontpage.
        if(is_front_page()){
            $this->context['is_frontpage'] = true;

            $coupons = new Coupons();
            $this->context['coupons'] = $coupons->get_coupons();
        }

        if(is_page('special-offers')) {
            $coupons = new Coupons();
            $this->context['coupons'] = $coupons->get_coupons();
        }

        if(is_singular('post')){
            $this->context['is_a_post'] = true;
        } else{
            $this->context['is_a_post'] = false;
        }

        $page_photo = get_field('page_photo', $this->context['post']->ID);
        $this->context['page_photo']['photo'] = $page_photo ? $page_photo : false;
        $this->context['page_photo']['add_link'] = get_field('add_link', $this->context['post']->ID);
        if($this->context['page_photo']['add_link']){
            $this->context['page_photo']['page_photo_link'] = get_field('page_photo_link', $this->context['post']->ID);
            $this->context['page_photo']['additional_link_attributes'] = get_field('additional_link_attributes', $this->context['post']->ID);
        }
    }

    /**
     * Figures out which view file is needed and renders it.
     */
    public function render_view(){
        if($this->view_file_exists()){
            $file = 'pages/' . $this->context['post']->slug . '.twig';
        } elseif(is_front_page()){
            $file = 'pages/home.twig';
        } elseif($this->is_posts()){
            $file = 'archive.twig';
        } elseif(is_404()){
            $file = '404.twig';
        } else{
            $file = 'single.twig';
        }

        Timber::render($file, $this->context, $this->cache);
    }
}