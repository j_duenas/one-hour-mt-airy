<?php

namespace icong;


class Coupon extends base{
    public  $id,
            $type = [],
            $price,
            $offer_text = null,
            $title = "New Coupon",
            $expiration_date,
            $description,
            $image;

    public function __construct($data){
        foreach($data as $key => $value){
            $this->setArg($key, $value);
        }

        $this->strip_amount_symbols();
    }

    /**
     * Removes $ and % from price.
     */
    protected function strip_amount_symbols(){
        $this->price = str_replace([
            '$',
            '%'
        ],'', $this->price);
    }
}