<?php
    global $params;
    $context = $params;
    $context['contact_info'] = get_field('contact_info', 'options');
    $context['form'] = TimberHelper::ob_function('the_form');
    Timber::render('partials/request-appointment-popup.twig', $context);

    function the_form(){
        echo FrmFormsController::get_form_shortcode( array( 'id' => 6, 'title' => false, 'description' => false ) );
    }