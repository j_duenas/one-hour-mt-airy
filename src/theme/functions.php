<?php
require_once(__DIR__ . '/vendor/autoload.php');
$timber = new Timber\Timber();

Routes::map('get-appointment-form', function($params){
    $params['contact_info'] = get_field('contact_info', 'options');
    Routes::load('appointment-form.php', $params);
});

/*TEMPORARY FOR PROOF SITE*/
Routes::map('onehour/get-appointment-form', function($params){
    $params['contact_info'] = get_field('contact_info', 'options');
    Routes::load('appointment-form.php', $params);
});

require 'theme_functions/load_acf.php';
require 'theme_functions/scripts.php';
require 'theme_classes/base.php';
require 'theme_classes/CPT.php';
require 'theme_classes/Coupon.php';
require 'theme_classes/Coupons.php';
require 'theme_classes/Review.php';
require 'theme_classes/Reviews.php';
require 'theme_classes/IconWebsite.php';

require 'theme_functions/custom_post_types/coupons.php';
require 'theme_functions/custom_post_types/reviews.php';
require 'theme_functions/menus.php';
require 'theme_functions/options.php';
require 'theme_functions/blog.php';
require 'theme_functions/shortcodes.php';
require 'theme_functions/coupons_hook.php';

function get_id_by_slug($page_slug)
{
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

