jQuery('#coupon-carousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 2,
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
    responsive: [
        {
            breakpoint: 1300,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 775,
            settings:{
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

jQuery(document.body).on('click', '.print-this', function(){
    var couponID = jQuery(this).data('print-element');
    jQuery('#' + couponID).printElement({
        printBodyOptions: {
            classNameToAdd: 'printed-coupon'
        },
        printMode: 'popup',
        leaveOpen: true
    });
    return false;
});