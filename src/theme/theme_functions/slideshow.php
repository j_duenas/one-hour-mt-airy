<?php

function icong_get_slides()
{
    $slides = array();
    $limit  = 0;
    if (have_rows('slides', 'options')) {
        while (have_rows('slides', 'option')) {
            the_row();
            if ((get_sub_field('sticky', 'option')) && ($limit !== 1)) {
                if (isset($slides[0])) {
                    $slides[] = array_shift($slides);
                }
                $slides[0] = get_sub_field('slide_image', 'option');
                $limit     = 1;

            } else {
                $slides[] = get_sub_field('slide_image', 'option');
            }

        }
    }

    return $slides;
}

?>
