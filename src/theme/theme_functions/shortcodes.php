<?php

function google_maps($atts){
    return Timber::compile('partials/shortcodes/google-map.twig');
}

add_shortcode('google-map', 'google_maps');

function reviews_list($atts){
    $reviews = new icong\Reviews();
    $context['reviews'] = $reviews->get_reviews();
    return Timber::compile('partials/shortcodes/reviews-list.twig', $context);
}

add_shortcode('reviews-list', 'reviews_list');