<?php

function ohah_scripts(){
    wp_enqueue_style('default-styles', get_stylesheet_uri());
    wp_enqueue_script('ohah_all', get_stylesheet_directory_uri() . '/all.js', array('jquery'), false, true);

    wp_enqueue_script('ohah_ajax_script', get_stylesheet_directory_uri() . '/js/coupons.js', array('jquery'), false, true);
    wp_localize_script('ohah_ajax_script', 'ohahajax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('ohah_ajax_script');
}

add_action('wp_enqueue_scripts', 'ohah_scripts');