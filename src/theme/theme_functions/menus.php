<?php

function icong_register_navigation()
{
    register_nav_menus(
        array(
            'header-nav'  => __('Header Menu'),
            'footer-nav-1' => __('Footer Menu, Column 1'),
            'footer-nav-2'  => __('Footer Menu, Column 2'),
        ));
}

add_action('init', 'icong_register_navigation');
