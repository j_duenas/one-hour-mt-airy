<?php

    $coupons = new \icong\customPostType();
    $coupons->setArg('id', 'coupons');
    $coupons->setArg('name_singular', 'Coupon');
    $coupons->setArg('name_plural', 'Coupons');
    $coupons->setArg('menu_icon', 'dashicons-tickets');
    $coupons->create();