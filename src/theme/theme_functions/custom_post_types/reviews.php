<?php

$coupons = new \icong\customPostType();
$coupons->setArg('id', 'reviews');
$coupons->setArg('name_singular', 'Review');
$coupons->setArg('name_plural', 'Reviews');
$coupons->setArg('menu_icon', 'dashicons-format-status');
$coupons->setArg('rewrite', false);
$coupons->create();