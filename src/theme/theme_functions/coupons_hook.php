<?php

function ohah_get_coupons(){
    check_ajax_referer('coupon_carousel_nonce', 'nonce');
    $coupons = new \icong\Coupons();
    $context['coupons'] = $coupons->get_coupons();
    if(isset($_REQUEST['carouselType']) && ($_REQUEST['carouselType'] === "desktop" || $_REQUEST['carouselType'] === "mobile")){
        $carousel_type = $_REQUEST['carouselType'];
    } else{
        $carousel_type = "desktop";
    }
    $html = Timber\Timber::compile('views/partials/frontpage/coupon-carousel-' . $carousel_type . '.twig', $context, false);

    echo $html;
    wp_die();
}
add_action('wp_ajax_nopriv_ohah_get_coupons', 'ohah_get_coupons');
add_action('wp_ajax_ohah_get_coupons', 'ohah_get_coupons');

