* ~~Find higher resolution logos for "family of home services"~~
* ~~Find correct background image for footer (metal w/ holes)~~
* ~~Replace 'Online Specials' tag image with properly transparent one~~
* ~~Create "Request Appointment" form~~
* ~~Create "contact us" form~~
* ~~Create "Products" pages~~
* ~~Create "Services" pages~~
* ~~Create "Ask an Expert" form (is it just the contact form??)~~
* ~~Add printing functionality to the coupons~~
* ~~Center "Call Our 24/7 Emergency Help Line" in header~~
* ~~Fix the fade (gradient)~~
* Find better logo for "Direct Energy"
* ~~Figure out the coupons (different slider, maybe?)~~
* ~~Create "Special Offers"~~
* ~~Create "Reviews" functionality~~
* ~~Replace "one hour guy" photo~~


>>Super low priority
* ~~Make slideshow us Slick instead of swipeshow~~


